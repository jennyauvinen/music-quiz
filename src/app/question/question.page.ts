import { Component, OnInit } from '@angular/core';
import { QuizService } from '../quiz.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})
export class QuestionPage implements OnInit {

  private feedback: string;
  private duration: number;
  private points: number;

  constructor(public router: Router, private quizService: QuizService) { }

  ngOnInit() {
    this.points = 0;
    this.quizService.addQuestions();
    this.quizService.initialize();
    this.feedback = "";
  }

  public getPoints() {
    return this.points;
  }

  private checkOption(option: number) {
    this.quizService.setOptionSelected();
    if (this.quizService.areAllOptionsUsed()) {
      this.quizService.setOptionSelected();
    }
    else {
      if (this.quizService.isCorrectOption(option)) {
        this.points++;
        this.feedback =
          this.quizService.getCorrectOptionOfActiveQuestion() +
          ' is correct!';
      }
      else {
        this.feedback = 'The answer is incorrect.';
      }
    }
  }

  private continue() {
    if (this.quizService.isFinished()) {
      this.points = this.getPoints();
      this.duration = this.quizService.getDuration();
      this.router.navigateByUrl('result/' + this.duration + '/' + this.points);
    }
    else {
      this.quizService.setQuestion();
    }
  }
}
