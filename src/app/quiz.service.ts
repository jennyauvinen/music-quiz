import { Injectable } from '@angular/core';
import { MusicQuestion } from '../musicquestion'

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  private questions: MusicQuestion[] = [];
  private activeQuestion: MusicQuestion;
  private isCorrect: boolean;
  private isSelected: boolean;
  private questionCounter: number;
  private optionCounter: number;
  private startTime: Date;
  private endTime: Date;
  private duration: number;
  public correctOptions: number;
  public message: string;

  constructor() { }

  public addQuestions() {
    this.questions = [{
      question: 'This band has hits like T.N.T, Thunderstuck, Who Made Who?',
      options: [
        'AC/DC',
        'Queen',
        'Led Zeppelin'],
      correctOption: 0
    },
    {
      question: 'Front man for Queen?',
      options: [
        'Freddie Mercury',
        'Robert Plant',
        'Stevie Ray Vaughan'],
      correctOption: 0
    },
    {
      question: 'Who is said to be "The Boss"?',
      options: [
        'Ozzy Osbourne',
        'Bruce Springsteen',
        'David Bowie'],
      correctOption: 1
    },
    {
      question: 'Which band members are known for their beards?',
      options: [
        'AC/DC',
        'ZZ Top',
        'Gun N\' Roses'],
      correctOption: 1
    }
    ];
  }

  public setQuestion() {
    this.optionCounter = 0;
    this.isCorrect = false;
    this.isSelected = false;
    this.activeQuestion = this.questions[this.questionCounter];
    this.questionCounter++;
  }

  public initialize() {
    this.questionCounter = 0;
    this.startTime = new Date();
    this.setQuestion();
  }

  public getQuestion(): MusicQuestion[] {
    return this.questions;
  }

  public getActiveQuestion(): MusicQuestion {
    return this.activeQuestion;
  }

  public getQuestionOfActiveQuestion(): string {
    return this.activeQuestion.question;
  }

  public getOptionsOfActiveQuestion(): string[] {
    return this.activeQuestion.options;
  }

  public getIndexOfActiveQuestion(): number {
    return this.questionCounter;
  }

  public getNumberOfQuestions(): number {
    return this.questions.length;
  }

  public getNumberOfOptionsOfActiveQuestion(): number {
    return this.activeQuestion.options.length;
  }

  public getIndexOfOptionCounter(): number {
    return this.optionCounter;
  }

  public getCorrectOptionOfActiveQuestion(): string {
    return this.activeQuestion.options[this.activeQuestion.correctOption];
  }

  public setOptionSelected() {
    this.isSelected = true;
  }

  public isOptionSelected(): boolean {
    return this.isSelected;
  }

  public areAllOptionsUsed(): boolean {
    this.optionCounter++;
    return (this.optionCounter > this.activeQuestion.options.length) ? true : false;
  }

  public isCorrectOption(option: number): boolean {
    this.isCorrect = (option === this.activeQuestion.correctOption) ? true : false;
    return this.isCorrect;
  }

  public isAnswerCorrect(): boolean {
    return this.isCorrect;
  }

  public isFinished(): boolean {
    return (this.questionCounter === this.questions.length) ? true : false;
  }

  public getDuration(): number {
    this.endTime = new Date();
    this.duration = this.endTime.getTime() - this.startTime.getTime();
    return this.duration;
  }
}
