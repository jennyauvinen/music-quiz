import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { QuizService } from '../quiz.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

  private correctness: number;
  private duration: number;
  private durationSeconds: number;
  private statusImage: string;
  private imagePath: string;
  public points: number;
  public message: string;

  constructor(public router: Router,
    public activatedRoute: ActivatedRoute,
    private quizService: QuizService) { }

  ngOnInit() {
    this.duration = Number(this.activatedRoute.snapshot.paramMap.get('duration'));
    this.durationSeconds = Math.round((this.duration) / 1000);
    this.points = Number(this.activatedRoute.snapshot.paramMap.get('points'));
    this.correctness = (this.points * 25);
  }

  private printResult() {
    if (this.points === 4 && this.durationSeconds < 35) {
      this.message = 'Brilliant!';
      this.imagePrint();
    } else if (this.points === 3 && this.durationSeconds < 45) {
      this.message = 'Very good!';
      this.imagePrint();
    } else if (this.points === 2 && this.durationSeconds < 55) {
      this.message = 'Ok!';
      this.imagePrint();
    } else if (this.points === 1 && this.durationSeconds < 65) {
      this.message = 'You need to listen more rock music.';
      this.imagePrint();
    } else {
      this.message = 'I guess you don\'t listen to rock music.';
      this.imagePrint();
    }
    return this.message;
  }

  private imagePrint() {
    return this.statusImage = '../../assets/imgs/' + this.points + '.png';
  }

  private goAnswers() {
    this.quizService.initialize();
    this.router.navigateByUrl('answers');
  }
}
