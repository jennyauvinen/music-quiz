export class MusicQuestion {
    question: string;
    options: string[];
    correctOption: number;
}